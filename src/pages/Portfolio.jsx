import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import { motion } from "framer-motion";
function Portfolio() {
  const [card, setCard] = useState([1, 2, 3]);
  const cardItems = card.map((item) => {
    return (
      <motion.div key={item} whileHover={{ scale: 1.1 }} style={{opacity:1, marginLeft:'20px', marginRight:'20px',marginBottom:'10px'}} >
        <Card>
          <Card.Body>
            <Card.Title>Card Title</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">
              Card Subtitle
            </Card.Subtitle>
            <Card.Text>
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nostrum necessitatibus ipsa natus magni eius, qui non unde atque aspernatur inventore recusandae fuga, dolores porro totam?
            </Card.Text>
            <Card.Link href="#">Card Link</Card.Link>
            <Card.Link href="#">Another Link</Card.Link>
          </Card.Body>
        </Card>
      </motion.div>
    );
  });

  return (
    <div className="vh-100 about-bg-color">
      <Container>
        <Row>
          <Col
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              marginTop: "3rem",
            }}
          >
            {cardItems}
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Portfolio;
