import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Figure from "react-bootstrap/Figure";
import HamiltonCycle from "../images/Hamiltonian Cycle.svg";
import Mobius from '../images/Mobius Transformation of Circles.svg'
import {motion} from "framer-motion"
function Interests() {
  return (
    <Container fluid className="about-bg-color vh-100">
      <Row>
        <Col className="col-md-8 interest-page-style ">
          <span>
            <h5 className="global-font text-white fs-4 fw-bolder px-5 py-5 mx-5 my-5">
              I love React, Js, Python and webdevelopment. Im an exprienced
              programmer with a lot of interests. Im also a physicists and like
              mathematics and physics.{" "}
            </h5>
          </span>
          <Figure>
            <Figure.Image
              width={300}
              height={300}
              alt="171x180"
              src={Mobius }
            />
          </Figure>
        </Col>

        <Col className="d-none d-md-block">
          <motion.Figure animate={{x:20}}>
            <Figure.Image
              width={400}
              height={400}
              alt="171x180"
              src={HamiltonCycle}
            />
          </motion.Figure>
        </Col>
      </Row>
    </Container>
  );
}

export default Interests;
