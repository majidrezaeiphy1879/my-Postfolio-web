import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import NavbarMenue from "./components/NavbarMenue";
import About from "./components/About";
import Portfolio from "./pages/Portfolio";
import Interests from "./pages/Interests";
import Contact from "./pages/Contact";
function App() {
  return (
    <BrowserRouter>
      <NavbarMenue />
      <Routes>
        <Route path="/" element={<About />} />
        <Route path="/portfolio" element={<Portfolio />} />
        <Route path="/interests" element={<Interests />} />
        <Route path="/contact" element={<Contact />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
