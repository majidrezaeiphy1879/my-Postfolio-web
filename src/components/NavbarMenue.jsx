import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";
function NavbarMenu() {
  const [activeButton, setActiveButton] = useState("/");
  const handleClick = (link) => {
    setActiveButton(link);
  };
  return (
    <Container fluid className="nav-bg-color">
      <Row>
        <Col xs={6}>
          <Navbar>
            <Container>
              <Navbar.Brand>
                <Link to="/" className="nav-brand">
                  Majid Rezaei
                </Link>
              </Navbar.Brand>
            </Container>
          </Navbar>
        </Col>
        <Col xs={6} className="d-flex justify-content-end">
          <Navbar expand="lg">
            <Navbar.Toggle aria-controls="basic-navbar-nav" className="mx-2" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ml-auto px-5">
                <Link
                  to="/"
                  className={`global-font navbar-links hidden-md-up px-2 mx-2 ${
                    activeButton === "/" ? "link-active-button" : ""
                  }`}
                  onClick={() => handleClick("/")}
                >
                  About
                </Link>
                <Link
                  to="portfolio"
                  className={`global-font navbar-links hidden-md-up px-2 mx-2 ${
                    activeButton === "/portfolio" ? "link-active-button" : ""
                  }`}
                  onClick={() => handleClick("/portfolio")}
                >
                  Portfolio
                </Link>
                <Link
                  to="interests"
                  className={`global-font navbar-links hidden-md-up px-2 mx-2 ${
                    activeButton === "/interests" ? "link-active-button" : ""
                  }`}
                  onClick={() => handleClick("/interests")}
                >
                  Interests
                </Link>
                <Link
                  to="contact"
                  className={`global-font navbar-links hidden-md-up px-2 mx-2 ${
                    activeButton === "/contact" ? "link-active-button" : ""
                  }`}
                  onClick={() => handleClick("/contact")}
                >
                  Contact
                </Link>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </Col>
      </Row>
    </Container>
  );
}

export default NavbarMenu;
