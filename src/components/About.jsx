import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import Figure from "react-bootstrap/Figure";
import gmailIcon from "../images/GmailIcon.svg";
import gitLabIcon from "../images/GitLabIcon.svg";
import linkedinIcon from "../images/linkedinIcon.svg";
import avatar from "../images/Avatar.jpeg";
import { motion } from "framer-motion";
function About() {
  const [cv, setCv] = useState({ isOn: false });
  const handleHover = () => {
    setCv((prevCv) => ({ isOn: !prevCv.isOn }));
  };
  return (
    <Container fluid className="about-bg-color">
      <Row>
        <Col></Col>
        <Col xs={6}>
          <motion.div
            className="about-page-style vh-100"
            animate={{ rotate: 360, opacity: 1 }}
            onHoverStart={handleHover}
            onHoverEnd={() => setCv({ isOn: false })}
          >
            {cv.isOn && (
              <a
                className="disapearing-link cv-download"
                href="http://localhost:5001/api/download-cv"
                rel="noreferrer"
                alt="cv"
                download
              >
                CV.PDF
              </a>
            )}
            <Image src={avatar} width={180} height={180} roundedCircle />

            <div className="about-intro-style">
              <h3>Majid Rezaei</h3>
              <span> Developer, Astrophysicist, Swimmer</span>
            </div>
            <div>
              <Figure style={{ marginTop: "15px" }}>
                <a
                  href="www.google.com"
                  style={{ textDecoration: "none", marginRight: "5px" }}
                  alt="gmail"
                  target="_blank"
                  rel="noreferrer"
                >
                  <Figure.Image
                    width={40}
                    height={40}
                    alt="171x180"
                    src={gmailIcon}
                  />
                </a>
                <a
                  href="https://gitlab.com/majidrezaeiphy1879"
                  style={{ textDecoration: "none" }}
                  alt="gitlab"
                  target="_blank"
                  rel="noreferrer"
                >
                  <Figure.Image
                    width={40}
                    height={40}
                    alt="171x180"
                    src={gitLabIcon}
                  />
                </a>
                <a
                  href="https://www.linkedin.com/in/majid-rezaei-phy"
                  target="_blank"
                  rel="noreferrer"
                  style={{ textDecoration: "none" }}
                  alt="linkedin"
                >
                  <Figure.Image
                    width={40}
                    height={40}
                    alt="171x180"
                    src={linkedinIcon}
                  />
                </a>
              </Figure>
            </div>
          </motion.div>
        </Col>
        <Col></Col>
      </Row>
    </Container>
  );
}

export default About;
