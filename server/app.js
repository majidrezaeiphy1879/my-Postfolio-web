const express = require("express");
const app = express();

const cors = require("cors");
app.use(cors());
const cvFile=require('./routes/cvRoute')
const contactForm = require('./routes/formRoute')

app.use(express.json()); 
app.use(express.urlencoded());


app.use('/api/download-cv', cvFile)
app.use('/api/contact', contactForm)


app.listen(5001, () => {
  console.log("Server is running on port 5001");
});
