const { Pool } = require('pg');

const pool = new Pool({
  user: 'postgres',
  password: '1984',
  host: 'localhost',
  port: 5432,
  database: 'node_react'
});

module.exports = {
  query: (text, params) => pool.query(text, params)
};