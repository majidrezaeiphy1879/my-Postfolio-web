const { Router } = require("express");
const router = Router();
const db = require("../db");
router.post("/", async (req, res) => {
  const { subject, message } = req.body;
  try {
    const insertQuery = "INSERT INTO backend.form_data (subject, message) VALUES ($1, $2)";
    
    await db.query(insertQuery, [subject, message]);

    res.json({ message: "Form data saved successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "An error occurred while saving the form data" });
  }
});

module.exports = router;
