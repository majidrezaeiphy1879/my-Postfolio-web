const {Router}=require('express')
const router=Router();
const path = require('path');

router.get('/', (req, res) => {
    const cvPath = path.join(__dirname, '../cv/myResume.pdf');
    res.download(cvPath, 'myResume.pdf');
  });
  
 module.exports=router;